<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("COMP_NAME"),
    "DESCRIPTION" => GetMessage("COMP_DESCR"),
    "COMPLEX"=> "Y",
    "PATH" => array(
        "ID" => "complex",
        "CHILD" => array(
            "ID" => "component",
            "NAME" => GetMessage("T_IBLOCK_DESC_COMP"),
            "SORT" => 10,
            "CHILD" => array(
                "ID" => "my_complex",
            ),
        ),
    ),
);

