<?
$MESS["T_IBLOCK_DESC_FILTER_SETTINGS"] = "Настройки фильтра";
$MESS["CN_P_LIST_SETTINGS"] = "Настройки списка";
$MESS["CN_P_DETAIL_SETTINGS"] = "Настройки детального просмотра";
$MESS["CN_P_DETAIL_PAGER_SETTINGS"] = "Настройки постраничной навигации детального просмотра";
$MESS["BN_P_SECTION_ID_DESC"] = "Идентификатор раздела";
$MESS["NEWS_ELEMENT_ID_DESC"] = "Идентификатор новости";
$MESS["T_IBLOCK_SEF_PAGE_NEWS"] = "Страница общего списка";
$MESS["T_IBLOCK_SEF_PAGE_NEWS_SECTION"] = "Страница раздела";
$MESS["T_IBLOCK_SEF_PAGE_NEWS_DETAIL"] = "Страница детального просмотра";
$MESS["BN_P_IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["BN_P_IBLOCK"] = "Инфоблок";
$MESS["T_IBLOCK_DESC_LIST_CONT"] = "Количество новостей на странице";
$MESS["T_IBLOCK_DESC_USE_FILTER"] = "Показывать фильтр";
$MESS["T_IBLOCK_DESC_CHECK_DATES"] = "Показывать только активные на данный момент элементы";
$MESS["T_IBLOCK_PROPERTY"] = "Свойства";
$MESS["T_IBLOCK_FILTER"] = "Фильтр";
$MESS["IBLOCK_FIELD"] = "Поля";
