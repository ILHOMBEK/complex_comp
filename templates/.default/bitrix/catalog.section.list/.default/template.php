<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach ($arResult['SECTIONS'] as $arSection)
{
    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
?>
    <div class="section">
        <div class="name">
            <a href="<?=$arSection["SECTION_PAGE_URL"]?>">
                <?=$arSection['NAME']?>
            </a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 fl-left">
                        <a href="<?=$arSection["SECTION_PAGE_URL"]?>">
                            <img src="<?=$arSection["PICTURE"]["SRC"]?>"/>
                        </a>
                    </div>
                    <div class="col-md-6 fl-right prev_text">
                        <p><?=$arSection['DESCRIPTION']?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}?>