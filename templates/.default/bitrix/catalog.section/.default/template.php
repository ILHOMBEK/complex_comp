<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach ($arResult['ITEMS'] as $arItem)
{
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strItemDelete, $arItemDeleteParams);
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strItemEdit);
?>
    <div class="section">
        <div class="name">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <?=$arItem['NAME']?>
            </a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6 fl-left">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"/>
                        </a>
                    </div>
                    <div class="col-md-6 fl-right prev_text">
                        <p><?=$arItem['PREVIEW_TEXT']?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}?>