<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6 fl-left">
                    <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"/>
                </div>
                <div class="col-md-6 fl-right prev_text">
                    <p><?=$arResult['DETAIL_TEXT']?></p>
                </div>
            </div>
        </div>
    </div>
</div>
