<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
    return;


$arIBlockType = CIBlockParameters::GetIBlockTypes();
$arIBlock=array();
$rsIBlock = CIBlock::GetList(array("sort" => "ASC"), array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch()){
     $arIBlock[$arr['ID']] = "[".$arr['ID']."]".$arr['NAME'];
}

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y",
    "IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"]));
while ($arr=$rsProp->Fetch())
{
    $arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
}

$arComponentParameters = array(
    "GROUPS" => array(
        "FILTER_SETTINGS" => array(
            "SORT" => 150,
            "NAME" => GetMessage("T_IBLOCK_DESC_FILTER_SETTINGS"),
        ),
        "LIST_SETTINGS" => array(
            "NAME" => GetMessage("CN_P_LIST_SETTINGS"),
        ),
        "DETAIL_SETTINGS" => array(
            "NAME" => GetMessage("CN_P_DETAIL_SETTINGS"),
        ),
        "DETAIL_PAGER_SETTINGS" => array(
            "NAME" => GetMessage("CN_P_DETAIL_PAGER_SETTINGS"),
        ),
    ),
    "PARAMETERS" => array(
        "VARIABLE_ALIASES" => Array(
            "SECTION_ID" => Array("NAME" => GetMessage("BN_P_SECTION_ID_DESC")),
            "ELEMENT_ID" => Array("NAME" => GetMessage("NEWS_ELEMENT_ID_DESC")),
        ),
        "SEF_MODE" => Array(
            "sections" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_NEWS_SECTION"),
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "section" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_NEWS"),
                "DEFAULT" => "#SECTION_ID#/",
                "VARIABLES" => array(
                    "SECTION_ID",
                    "SECTION_CODE",
                    "SECTION_CODE_PATH",
                ),
            ),
            "element" => array(
                "NAME" => GetMessage("T_IBLOCK_SEF_PAGE_NEWS_DETAIL"),
                "DEFAULT" => "#SECTION_ID#/#ELEMENT_ID#/",
                "VARIABLES" => array(
                    "ELEMENT_ID",
                    "ELEMENT_CODE",
                    "SECTION_ID",
                    "SECTION_CODE",
                    "SECTION_CODE_PATH",
                ),
            ),
        ),
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("BN_P_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("BN_P_IBLOCK"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
            "ADDITIONAL_VALUES" => "Y",
        ),
        "NEWS_COUNT" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_IBLOCK_DESC_LIST_CONT"),
            "TYPE" => "STRING",
            "DEFAULT" => "20",
        ),
        "USE_FILTER" => Array(
            "PARENT" => "FILTER_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_DESC_USE_FILTER"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "REFRESH" => "Y",
        ),
        "CHECK_DATES" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("T_IBLOCK_DESC_CHECK_DATES"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "LIST_PROPERTY_CODE" => array(
            "PARENT" => "LIST_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_PROPERTY"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arProperty_LNS,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "DETAIL_PROPERTY_CODE" => array(
            "PARENT" => "DETAIL_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_PROPERTY"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arProperty_LNS,
            "ADDITIONAL_VALUES" => "Y",
        ),
    ),
);


if($arCurrentValues["USE_FILTER"]=="Y")
{
    $arComponentParameters["PARAMETERS"]["FILTER_NAME"] = array(
        "PARENT" => "FILTER_SETTINGS",
        "NAME" => GetMessage("T_IBLOCK_FILTER"),
        "TYPE" => "STRING",
        "DEFAULT" => "",
    );
    $arComponentParameters["PARAMETERS"]["FILTER_FIELD_CODE"] = CIBlockParameters::GetFieldCode(GetMessage("IBLOCK_FIELD"), "FILTER_SETTINGS");
    $arComponentParameters["PARAMETERS"]["FILTER_PROPERTY_CODE"] = array(
        "PARENT" => "FILTER_SETTINGS",
        "NAME" => GetMessage("T_IBLOCK_PROPERTY"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arProperty_LNS,
        "ADDITIONAL_VALUES" => "Y",
    );
}